import subprocess
import re
import requests
import xmltodict
import dicttoxml

from datetime import date, datetime
from fastapi import FastAPI, Response
from typing import Optional
from bs4 import BeautifulSoup

app = FastAPI(
    title="7awi Horoscope APIs",
    description="7awi Horoscope APIs for Viber Integration",
    version="0.1.0"
    )

@app.get('/horoscopes')
async def horoscopes(day: Optional[date] = '', json: Optional[bool] = False, parsed: Optional[bool] = True, separator: Optional[str] = 'n'):
    if day == '':
        day = datetime.today().strftime('%Y-%m-%d')

    TITLE_KEYWORDS = 'أبراج اليوم'
    NUMBER_OF_SIGNS = 12

    # url = 'https://feeds.7awi.com/layalina/all_with_video/%D8%A3%D8%A8%D8%B1%D8%A7%D8%AC-abraj/?client=flipboard&key=9eab6d8032e5856d20cb3b59f26fa512c2c72504'
    url = "https://feeds.7awi.com/layalina/all_with_video/%D8%A8%D8%B1%D8%AC-%D8%A7%D9%84%D9%8A%D9%88%D9%85/?client=flipboard&key=9eab6d8032e5856d20cb3b59f26fa512c2c72504"

    response = requests.get(url)
    horoscopes = xmltodict.parse(response.content)['rss']['channel']['item']

    if day != '':
        horoscopes = list(filter(lambda horoscope: datetime.strptime(horoscope['pubDate'], '%a, %d %b %Y %H:%M:%S +0000').strftime('%Y-%m-%d') == str(day), horoscopes))

    horoscopes = list(filter(lambda horoscope: TITLE_KEYWORDS in horoscope['title'], horoscopes))

    if not parsed:
        return horoscopes

    horoscopes_response = {
        "horoscopes": {
            "@generatedAt": datetime.now(),
            "horoscope": {
                "signs": {
                    "sign": []
                }
            }
        }
    }

    horoscopes_response['horoscopes']['horoscope']['@day'] = day
    for horoscope in horoscopes:
        content = horoscope['content:encoded']
        link = horoscope['link']

        signs = content.split('<h2><a id=')
        signs = signs[1:]

        for i in range(len(signs)):
            current_sign = i + 1

            sign_item = '<h2><a id=' + signs[i]
            
            content = BeautifulSoup(sign_item)
            title = content.find('a', attrs={'id': current_sign}).text.replace('توقعات برج', '').replace('اليوم', '').replace(f'{current_sign}. ', '').strip()
            
            if separator == 'br':
                text = "<br />".join([tag.text for tag in content.find_all('p')][:5])
            elif separator == 'p':
                text = '\n'.join(map(lambda t: f'<p>{t}</p>',[tag.text for tag in content.find_all('p')][:4]))
            else:
                text = "\n".join([tag.text for tag in content.find_all('p')][:5])

            if current_sign == 11:
                print(sign_item)
            
            url = f'{link}#{current_sign}' #content.find('a', href=True)['href']
            sign_en = get_sign_en(title)
            
            horoscopes_response['horoscopes']['horoscope']['signs']['sign'].append({
                "@id": current_sign,
                "@strId": sign_en,
                "@lang": "ar",
                "caption": title,
                "text": text,
                "url": url
            })

    if json:
        return horoscopes_response

    horoscopes_response = xmltodict.unparse(horoscopes_response, pretty=True)

    return Response(content=horoscopes_response, media_type="application/xml")

sign_map = {
    "الحمل": "aries",
    "الثور": "taurus",
    "الجوزاء": "gemini",
    "السرطان": "cancer",
    "الأسد": "leo",
    "العذراء": "virgo",
    "الميزان": "libra",
    "العقرب": "scorpio",
    "القوس": "sagittarius",
    "الجدي": "capricorn",
    "الدلو": "aquarius",
    "الحوت": "pisces"
}

def get_sign_en(sign):
    return sign_map[sign]
